import nbformat
import json
import re


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def read_validation_config():
    with open('validation.json') as f:
        data = json.load(f)
    return data


def read_notebook_cells(path_to_notebook):
    code_cells = []
    nb = nbformat.read(path_to_notebook, as_version=4)
    i_nb = iter(nb.items())
    cells = next(i_nb)[1]

    for c in cells:
        if c['cell_type'] == 'code' and 'execute' in c['metadata'].keys():
            code_cells.append(c)

    return code_cells


def validate_answer(regex, cell_data):
    global is_complete
    pattern = re.compile(regex)
    is_match = bool(re.search(pattern, cell_data))
    
    if is_match:
        print(bcolors.OKGREEN + "Correct" + bcolors.ENDC)
    else:
        print(bcolors.FAIL + "Wrong" + bcolors.ENDC)
        is_complete = False

    return is_match


cells = read_notebook_cells('test.ipynb')
# validation_config = read_validation_config()

validation_config = {
    "flag":"abc",

    "rules": [
        {
            "id": 0,
            "code_regex": "money(\\s)?=(\\s)?26",
            "output_regex": "(26)?"
            
        },

        {
            "id": 1,
            "code_regex": "",
            "output_regex": "(11)?"
        },

        {
            "id": 2,
            "code_regex": "money(\\s)?/(\\s)?0",
            "output_regex": "(.|\n)ZeroDivisionError(.|\n)"
        },

        {
            "id": 3,
            "code_regex": "(.|\n)?print\\([a-zA-Z_0-9]*(\\s)?\\*(\\s)?53\\)(.|\n)?",
            "output_regex": ""
        },

        {
            "id": 4,
            "code_regex": "(.|\n)?len\\(words\\)(.|\n)?",
            "output_regex": ""
        }

    ]
}

flag = validation_config['flag']
is_complete = True


cell_num = 0

for c in cells:
    print('*********CODE***********')
    print(c['source'])

    # validate code here
    validate_answer(validation_config['rules'][cell_num]['code_regex'], c['source'])

    print('************************')
    print('--------OUTPUT----------')
    
    if 'outputs' in c.keys() and len(c['outputs']) > 0:

        if 'text' in c['outputs'][0].keys():
            print(c['outputs'][0]['text'].strip())

            # validate output here
            validate_answer(validation_config['rules'][cell_num]['output_regex'], c['outputs'][0]['text'].strip())
            

        elif c['outputs'][0]['output_type'] == 'error':
            print(''.join(c['outputs'][0]['traceback']))

            # validate output here
            validate_answer(validation_config['rules'][cell_num]['output_regex'], ''.join(c['outputs'][0]['traceback']))


        elif 'text/plain' in c['outputs'][0]['data'].keys():
            print(c['outputs'][0]['data']['text/plain'].strip())
            
            # validate output here
            validate_answer(validation_config['rules'][cell_num]['output_regex'], c['outputs'][0]['data']['text/plain'].strip())

    else:
        print('NO OUTPUT')
        if validation_config['rules'][cell_num]['output_regex']:
            
            print(bcolors.FAIL + "Wrong" + bcolors.ENDC)
        else:
            print(bcolors.OKGREEN + "Correct" + bcolors.ENDC)


    print('------------------------')
    print('\n\n')
    cell_num += 1


if is_complete:
    print(flag)
else:
    print('Please try again')


